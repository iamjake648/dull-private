// Load the rich text editor
var inscrybmde = new InscrybMDE({
	element: document.getElementById("message-compose"),
	lineWrapping: true,
	minHeight: "100px",
	placeholder: "Type here...",
	promptURLs: true,
	promptTexts: {
		image: "Custom prompt for URL:",
		link: "Custom prompt for URL:",
	},
	spellChecker: true
});

function getSpecialContentDiv(type, id, content){
		var html = "<div class='message-extra message-extra-"+type+"' id='"+id+"-"+type+"'>";
			html += content;
		html += "</div>";
		return html;
}

function getSummaryDiv(link, summary, footerContent, image, id){
  var html = "<div class='link-summary' id='"+id+"-link-summary'>";
		//Add image if there is one
		if (image != null){
			html += getImageFromUrl(image);
		}
		html += "<div class='link-summary-body'>" + summary + "</div>";
		html += "<div class='link-summary-footer'>" + footerContent + "</div>";
	html += "</div>";
	return html;
}

function parseForMentions(messageObject, id){
	//Check if anyone was mentioned
	if (messageObject.mentionedPeople != null && messageObject.mentionedPeople.length > 0){
		console.log(messageObject);
		//if there was, get the rest of their user information
		messageObject.mentionedPeople.forEach(function(personId){
			console.log(personId);
			//get their user information
			usersStorage.find({ id : personId }, function (err, docs) {
				console.log(docs);
				//If we have data for them
				if (docs.length > 0){
					//Try to find their name in the message
					if (messageObject.text.includes(docs[0].firstName)){
						//Match their name in the message
						var regexp = new RegExp(docs[0].firstName, "gi");
						//Replace it with highlighting
						var withHighlighting = messageObject.text.replace(regexp, "<span class='mentioned-user' id=''>@"+docs[0].firstName+"</span>");
						console.log(withHighlighting);
						$("#" + id).html(withHighlighting);
					}
				}
			});
		});
	}
}

function parseForSpecialContent(messageContent, id){
	//First check for various types of links
	var urlRegex =/(\b(https?|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
	//Test first so we don't parse if we don't need to
	if (urlRegex.test(messageContent)){
		//Split up the various links in the message
		var links = messageContent.match(urlRegex);
		links.forEach(function(link){
			//If it's a video site, include embed links
			if (messageContent.startsWith('https://www.youtu')){
				$("#" + id).append(getSpecialContentDiv('video',id,getYouTubeEmbedCode(link)));
			} else if (messageContent.startsWith('https://vimeo')){
				$("#" + id).append(getSpecialContentDiv('video',id,getVideoVimeoURL(link)));
			} else
				//First check if it's an image async, if not callback will do regular link summary
				regularLinkTester(link,id);
		});
	}
}

function renderUserSearchResults(usersArray){
	//Clear the results
	$("#new-chat-search-results").html('');
	//For every user
	usersArray.forEach(function(user){
		$("#new-chat-search-results").append(getUserDiv(user));
	});
}

function setChatVisible(roomId){
	roomsStorage.find({ id: roomId }, function (err, docs) {
		console.log(docs);
		//Update the room to invisible
		docs[0].is_visible = true;
		roomsStorage.update({ id: roomId }, docs[0], {}, function (err, numReplaced) {
			console.log('Set visible');
		});
	});
}

function startNewChat(roomId, name){
	//remove the new user modal
	$('#newChatModal').modal('toggle');
	//First check if this is already in the sidebar
  if (($("#" + roomId + "-title").length == 0)){
		roomsStorage.find({ title: name }, function (err, docs) {
			//set chat is_visible
			setChatVisible(docs[0].id);
			$("#chats-list").append(getChatHTML(docs[0].id, name));
			renderMessagePane(docs[0].id);
		});
	} else {
		//Just render the existing window
	}
}

function getUserDiv(userObject){
	console.log(userObject);
	return "<div id='user-result"+userObject.id+"' onclick='startNewChat(\""+userObject.id+"\",\""+userObject.displayName+"\");' class='user-result-item'><div class='user-avatar user-"+userObject.status+"'><img class='user-avatar-image' src='" + userObject.avatar + "'/><span class='user-name'>" + userObject.displayName + "</div>";
}

function renderRoomNameAndDetails(roomId){
	roomsStorage.find({ id: roomId }, function (err, docs) {
		$("#header-person-name").html(docs[0].title);
		//Use a group icon
		$("#header-person-photo").html("<img class='user-avatar-image' src='images/group.png'/>");
	});
}

function setSidebarNotification(roomId, count){
	var existingNotificaton = $("#" + roomId + "-notifiy-badge").html();
	if (existingNotificaton != null && existingNotificaton != ''){
		$("#"+roomId+"-notifiy-badge").html((parseInt(existingNotificaton) + count));
	} else {
		$("#"+roomId+"-notifiy-badge").html(count);
	}
}

function renderMessagePane(roomId){
  if (roomId.includes('-')){
    roomId = roomId.split('-')[0];
  }
	console.log('render: ' + roomId);
	window.activeChatId = roomId;
	getRoomMemberships(roomId);
	//Show the loading screen and hide the content
	$("#messages-loading-pane").show();
	$("#messages-content-pane").hide();
	$("#messages-content-pane").html('');
	messagesStorage.find({ roomId : roomId}).sort({created: 1}).exec(function (err, messages) {
		//if no messages are found, just show a blank chat.
		if (messages.length == 0){
			$("#messages-loading-pane").hide();
			$("#messages-content-pane").show();
			return;
		}
		//Only scroll after all of the messages have been loaded
		var messageCount = 0;
		var myId = sessionStorage.getItem("logged_in_user_id");
		messages.forEach(function(message){
			//Return a max of x number of messages defined by the user
			if (messageCount > window.maxChatCount){
				return;
			}
			//Don't render it if it's already in the view
			if (($("#" + message.id).length != 0)){
				//Could be from a message being sent or somethign along those lines
				return;
			}
			var isMe = (myId == message.personId);
			//Get the person details, query API if they aren't in database
			usersStorage.find({ id : message.personId }, function (err, docs) {
				//Check if we can use markdown, if not use text
				var body = message.markdown;
				//Default back to text
				if (body == null || body == ''){
					body = message.text;
				}
				//if it's stored, use that version
				if (docs.length > 0){
					$("#messages-content-pane").append(getChatItemHTML(body,docs[0].displayName, message.created, message.id,isMe));
				//It's not stored, get it from the API and store it
				} else {
					$("#messages-content-pane").append(getChatItemHTML(body,'Unknown (Try Refreshing)', message.created, message.id,isMe));
				}
				//Parse this link for special content like videos, emoticons, etc - added async
				parseForSpecialContent(message.text, message.id);
				//Check for mentions
				parseForMentions(message, message.id);
				//We need to check for the current message since db access is async
				if (messageCount == messages.length -1){
					//Hide the loading screen
					$("#messages-loading-pane").hide();
					$("#messages-content-pane").show();
					$("#messages-pane").scrollTop($("#messages-pane")[0].scrollHeight);
					//REnder any code blocks
					$('pre code').each(function(i, block) {
	 					hljs.highlightBlock(block);
 					});
				}
				messageCount++;
			});
		});
	});
}

function getChatHTML(id, title){
  var roomHTML = "<div id='"+id+"-active-dot'></div><div id='"+id+"' class='chat-list-item user-chat text'>";
    roomHTML += "<span id='"+id+"-title' class='chat-list-title'>"+title+"</span>";
  	roomHTML += "<span class='notify-badge' id='"+id+"-notifiy-badge'></span><div id='"+id+"-remove-button' class='remove-button'></div>";
  return roomHTML;
}

function renderRooms(roomsList){
	// $("#chats-list").html('');
	// $("#rooms-list").html('');
	roomsList.forEach(function(room){
    //first check if it needs to be appended, or already exists
    if (($("#" + room.id).length == 0) && room.is_visible){
      //Needs to be addded
      if (room.type == "direct"){
  			$("#chats-list").append(getChatHTML(room.id, room.title));
  		} else {
  			$("#rooms-list").append(getChatHTML(room.id, room.title));
  		}
			//Setup listener for rendering chats
			$("#" + room.id).click(function(event){
				renderMessagePane(event.target.id);
				//Remove any notifications that room had built up since it's opening
				$("#" + room.id + "-notifiy-badge").html('');
			});
			//Add a listner to the
			$("#" + room.id + "-remove-button").click(function(event){
				setChatHidden(event.target.id.split('-')[0]);
			});
    }
	});
}

function setChatHidden(roomId){
	//set it to hidden and remove it from the sidebar
	roomsStorage.find({ id: roomId }, function (err, docs) {
		console.log(docs);
		//Update the room to invisible
		docs[0].is_visible = false;
		roomsStorage.update({ id: roomId }, docs[0], {}, function (err, numReplaced) {
			//remove from the sidebar
			$("#" + roomId).remove();
			$("#" + roomId + "-active-dot").remove();
		});
	});

}

//Used when sending a message so the whole pane doesn't need to be re-rendered
function addTempChatHTML(text,isMe,messageId,personId){
	if (personId != null){
		usersStorage.find({ id: personId }, function (err, docs) {
			console.log(docs);
			$("#messages-content-pane").append(getChatItemHTML(text,docs[0].displayName,null,messageId,isMe));
			$("#messages-pane").scrollTop($("#messages-pane")[0].scrollHeight);
		});
	} else {
		settingsStorage.find({ setting_type: "logged_in_user" }, function (err, docs) {
			$("#messages-content-pane").append(getChatItemHTML(text,docs[0].displayName,null,messageId,isMe));
			$("#messages-pane").scrollTop($("#messages-pane")[0].scrollHeight);
		});
	}

}

function getChatItemHTML(text, sender, time, messageId, isLoggedInUser){
	var d = new Date();
	if (time != null){
		d = new Date(time);
	}
	var date = (d.getMonth() + 1)	+ "-" + d.getDate();
	var tod = d.getHours() + ":" + d.getMinutes();
	var dateTime = " " + date + " " + tod;
	var classForMessage = "logged-in-chat-user";
	if (!isLoggedInUser){
		classForMessage = "other-chat-user";
	}
	var html = "<div class='chat-message "+classForMessage+"' id='"+messageId+"'><div class='message-header'><span class='message-sender text'>"+sender+"</span><span class='message-time text'>"+dateTime+"</span></div>";
		html += "<div class='message-content'>"+urlify(text)+"</div>";
		html += "</div>";
	return html;
}
