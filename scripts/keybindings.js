//Register the Utility Namespace
var Utility = Utility || {};

/**
* @MethodName : Utility.RegisterKeyPress
* Will register a click event for multiple keys. Provide
* the item to be registered, the keycodes and then if it's a modal or not.
* If there is a modal, it will be toggled, else element is clicked.
*
* @Example : Utility.RegisterKeyPress('#btnSubmit', 78, 91, true);
*/
Utility.RegisterKeyPress = function (ctrl, firstKeyCode, secondKeyCode, isModal) {
    var codeset = {  };
		//Define it like this since the keys are dynamic
		codeset[firstKeyCode] = false;
		codeset[secondKeyCode] = false;
    //Tracking the Key down & Key up events
    $(document).on('keydown', function (e) {
        if (e.keyCode in codeset) {
            codeset[e.keyCode] = true;
						//Command + n for a new chat window
            if (codeset[firstKeyCode] && codeset[secondKeyCode]) {
							if (isModal){
								$(ctrl).modal('toggle');
							} else {
								$(ctrl).click();
							}
            }
        }
    }).on('keyup', function (e) {
        if (e.keyCode in codeset) {
            codeset[e.keyCode] = false;
        }
    });
};


$(document).on('keydown', function(e) {
	if (e.which == "13"){
			sendNewMessage();
	}
	//Command n should register new chat window
	//Utility.RegisterKeyPress("#newChatModal", 78, 91, true);
});
