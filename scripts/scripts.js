if (!("refresh_rate" in localStorage)){
	localStorage.setItem("refresh_rate", 15);
}

const {ipcRenderer} = require('electron')
Notification = require('node-mac-notifier');

window.activeChatId;
window.activeChatType = 'direct'; //direct or group
window.useToPersonId = false;
window.clientOpen = true;
window.maxChatCount = 100;
//TODO LOAD THESE FROM SREVER UPON LAUNCH
window.emoticons = {};
emoticons["(starcraft)"] = "https://s3.amazonaws.com/uploads.hipchat.com/72459/676253/3XP2GTIjbpHVEfm/Scafide%20-%20Head.png";

//First check if access token is already in the session
if (!("accessToken" in sessionStorage)){
	//Setup the API Key passed from the electron main process
	var uri = decodeURIComponent(window.location.hash);
	var obj = uri.substring(1, uri.length);
	var apiKey = JSON.parse(obj).apiKey;
	//Get the token sync because it's needed for the rest of the app
	var reqBody = { "grant_type" : "authorization_code", "client_id" : "C2847889489e42e88cc62902e727a72a0da862e08f400b4851af92efabe346c54", "client_secret" : "b6e26f9eb4d3940f3ffdb2b47cddeaa1df2d8ac7d4d69fecbb86db5d0f27bb0b", "code" : apiKey, "redirect_uri" : "http://localhost/oauthsuccess" };
	var tokenRequest = getAuthRequestObject(reqBody);
	tokenRequest.done(function(token){
		console.log(token);
		//Store the token in the session
		sessionStorage.setItem("accessToken", token.access_token);
		//Load the rest of the app
		init();
  })
  .fail(function(xhr){
    console.log(xhr);
  });
} else {
	//Just load the app
	init();
}

function init(){
	//Store details about the current logged in user
	getLoggedInUserDetails();
	//Every x seconds
	window.setInterval(function(){
  	getRooms();
		roomsStorage.find({}).sort({created: -1}).exec(function (err, docs) {
			renderRooms(docs);
		});
	}, (localStorage.getItem("refresh_rate") * 1000));
	//Register listener for seraching
	$("#new-chat-search").keydown(function(e){
		if ($("#new-chat-search").val().length > 1){
			searchForUser($("#new-chat-search").val());
		}
	});

}

function searchForUser(name){
	var reqBody = { "displayName" : name};
	var userRequest = getRequestObject("https://api.ciscospark.com/v1/people", reqBody, "GET");
	userRequest.done(function(users){
		//Render the users to the search window
		renderUserSearchResults(users.items);
		//Upsert all these items so we have them stored later for other things
		users.items.forEach(function(u){
			usersStorage.update({id: u.id }, u, { upsert: true });
		});
	})
	.fail(function(xhr){
		console.log(xhr);
	});
}

function sendMessage(content, roomId){
	//Don't send blank messages
	if (content == null || content == ""){
		return;
	}
	var reqBody;
	if (window.useToPersonId){
		reqBody = { "markdown" : content, "toPersonId" : roomId};
		window.useToPersonId = false;
	} else {
		reqBody = { "markdown" : content, "roomId" : roomId};
	}
	//Add to window right away so there is no delay
	addTempChatHTML(content,true,null,null);
	inscrybmde.value('');
	//Post the message to the api
	var newMessage = getRequestObject("https://api.ciscospark.com/v1/messages", JSON.stringify(reqBody), "POST");
	newMessage.done(function(token){
		console.log(token);
	})
	.fail(function(jqXHR, textStatus, errorThrown){
		console.log(jqXHR);
		console.log(textStatus);
		console.log(errorThrown);
	});
}

function sendNewMessage(){
	sendMessage(inscrybmde.value(),window.activeChatId);
}

function getLoggedInUserDetails(){
	var meRequest = getRequestObject("https://api.ciscospark.com/v1/people/me", null, "GET");
	meRequest.done(function(me){
		//Set the settings type before inserting, gives us a key for later
		me["setting_type"] = "logged_in_user";
		//Store this for fast access
		sessionStorage.setItem("logged_in_user_id", me.id);
		settingsStorage.update({setting_type: "logged_in_user" }, me, { upsert: true });
		//Also put it in the user storage for mentions
		usersStorage.update({id: me.id }, me, { upsert: true });
	})
	.fail(function(xhr){
		console.log(xhr);
	});
}

//Load all of the settings and user data
var roomsStorage = new Nedb({ filename: 'dull-rooms', autoload: true });
//roomsStorage.remove({}, { multi: true });
var messagesStorage = new Nedb({ filename: 'dull-messages', autoload: true });
var usersStorage = new Nedb({ filename: 'dull-users', autoload: true });
//messagesStorage.remove({}, { multi: true });
//usersStorage.remove({}, { multi: true });
var settingsStorage = new Nedb({ filename: 'dull-settings', autoload: true });

//Functions to get the latest messages and
function getRooms(){
	var roomsRequest = getRequestObject('https://api.ciscospark.com/v1/rooms', null, 'GET');
	roomsRequest.done(function(rooms){
		//Check if there has been more activity in this room
		rooms.items.forEach(function(roomFromAPI){
			//If there is new activity, get messages for this room
			roomsStorage.find({ id: roomFromAPI.id }, function (err, docs) {
				//Get memberships for this room
				getAllUsersInRoom(roomFromAPI.id, null);
				//console.log(docs);
				//Check if something is saved for this room already
				var shouldGetMessages = true;
				var lastActivityDate = null;
				if (docs.length > 0){
					lastActivityDate = docs[0].lastActivity;
				}
				//if there are no new updates, and we have a saved room already, don't get new messages
				if (docs.length > 0 && roomFromAPI.lastActivity <= docs[0].lastActivity){
					shouldGetMessages = false;
				}
				//If there are new messages, get them
				if (shouldGetMessages){
					getRoomMessages(roomFromAPI.id, lastActivityDate);
				}
				//Never overwrite the is visible property, or set it for the first time
				if (docs.length > 0){
					if (docs[0].is_visible != null){
						//It already exists, so don't overwrite
						roomFromAPI.is_visible = docs[0].is_visible;
					} else {
						//Property isn't set, default to true
						roomFromAPI.is_visible = true;
					}
				//First time insert, set it visible in the chat
				} else {
					roomFromAPI.is_visible = true;
				}
				//Upsert the room from the API
				roomsStorage.update({id: roomFromAPI.id }, roomFromAPI, { upsert: true });
			});
		});
  	})
  	.fail(function(xhr){
    	console.log(xhr);
  	});
}

function getRoomMessages(roomId, lastActivityDate){
	//First url will get the newest results, then paged
	var messagesURL = "https://api.ciscospark.com/v1/messages";
	var messagesToInsert = [];
	//Later calls will be to paged API if more than 50 results returned
	var reqBody = { "roomId" : roomId };
	getRoomMessagesAfterDateRecursive(roomId, lastActivityDate, messagesURL, messagesToInsert, reqBody);
}

function getRoomMessagesAfterDateRecursive(roomId, lastActivityDate, url, toInsert, body){
	//If the last acitivy is null, get all messages for the room
	var getAllMessages = (lastActivityDate == null);
	var messagesRequest = getRequestObject(url, body, 'GET');
	messagesRequest.done(function(messagesFromAPI, textStatus, xhr){
		var foundLast = false;
		//loop until we just found new messages
		var i = 0;
		while (!foundLast && i < messagesFromAPI.items.length){
			if (!getAllMessages && messagesFromAPI.items[i].created <= lastActivityDate){
				//We found the last one we should get, stop looping
				foundLast = true;
			} else {
				//Insert it, it's a new message
				toInsert.push(messagesFromAPI.items[i]);
			}
			i++;
		}
		//If we didn't find the last saved index, make a request to next
		if (!foundLast && xhr.getResponseHeader('Link') != null){
			var link = xhr.getResponseHeader('Link');
			var nextPage = link.substring(link.indexOf('<')+1, link.indexOf('>'));
			//Link holds the next page from the api, don't send body when getting next page
			getRoomMessagesAfterDateRecursive(roomId, lastActivityDate, nextPage, toInsert,null);
		} else {
			if (toInsert.length > 0){
				handleNewMessages(toInsert, roomId);
			}
		}
	})
	.fail(function(xhr){
    	console.log(xhr);
  	});
}

function handleNewMessages(toInsert, roomId){
	//Insert the messages, then update the view
	var myId = sessionStorage.getItem("logged_in_user_id");
	insertNewMessages(toInsert);
	//Send native notification if the chat window isn't open
	if (!window.clientOpen){
		//Show a native notification
		console.log('Shwo notifications');
		showNotifications(toInsert);
		//Rerender if this chat is open in the background
		if (window.activeChatId == roomId){
			renderMessagePane(roomId);
		}
		var messagesToSend = [];
		toInsert.forEach(function(message){
			var isMe = (myId == message.personId);
			//Only show notifications for messages that aren't you
			if (!isMe){
				messagesToSend.push(messages);
			}
		});
		if (messagesToSend.length > 0){
			//Set the badge icon and bounce it
			ipcRenderer.send("notification-data", messagesToSend);
			//Also update the chat client with notification count
			setSidebarNotification(roomId, messagesToSend.length);
		}
	} else if (window.activeChatId == roomId) {
		//add to the screen instead of refreshing the whole client
		toInsert.forEach(function(m){
			//Make sure it's not already in the view
			if (($("#" + m.id).length == 0)){
				var isMe = (myId == m.personId);
				//Only add it to the view if it's not you - if you sent it, it would already be in the view
				if (!isMe){
					addTempChatHTML(urlify(m.text),isMe,m.id,m.personId);
				}
				parseForSpecialContent(m.text, m.id);
			}
		});
	} else {
		//Add a notification to the sidebar
		setSidebarNotification(roomId, toInsert.length);
	}
}


function urlify(text) {
		if (text == null || text == ''){
			return text;
		}
		//Make links
    var urlRegex = /(https?:\/\/[^\s]+)/g;
    var withLinks = text.replace(urlRegex, function(url) {
        return '<a href="' + url + '" target="_blank">' + url + '</a>';
    });
		withLinks = marked(withLinks);
		//Now parse emojis
		var emojiRegex = /\(\S+\)/g;
		return withLinks.replace(emojiRegex, function(text) {
				//There is an emoticon for this text
				if (text in window.emoticons){
					//Get the url for this emoticon
					return '<span class="emoticon" alt="'+text+'"><img class="emoticon" alt="'+text+'" src="'+window.emoticons[text]+'"/></span>';
				} else {
					//Just return the text back
					return text;
				}
    });
}

function renderAndShowMainWindow(roomId){
	//Render the chat client
	renderMessagePane(roomId);
	//Bring the main window to the front
	ipcRenderer.send("show-window", true);
}

function showNotifications(messageList){
	var myId = sessionStorage.getItem("logged_in_user_id");
	messageList.forEach(function(msg) {
		//Only show notifications if they aren't for yourself
		if (msg.personId != myId){
			//Check if it's a room or individual and get their name
			if (msg.roomType == "group"){
				//Get the room name
				roomsStorage.find({ id: msg.roomId }, function (err, docs) {
					//setup the object and listener
					noti = new Notification(docs[0].title, {body: msg.text, canReply : true, id : msg.id, icon : "https://image.freepik.com/free-icon/group-profile-users_318-41953.jpg"});
					//Add listners to allow reply and quickly go to the chat
					noti.addEventListener('click', () => renderAndShowMainWindow(msg.roomId));
					noti.addEventListener('reply', ({response}) => sendMessage(response, msg.roomId));
				});
			} else {
				//Get the user's name
				usersStorage.find({ id: msg.toPersonId }, function (err, docs) {
					noti = new Notification(docs[0].displayName, {body: msg.text, canReply : true, id : msg.id, icon : docs[0].avatar});
					//Add listners to allow reply and quickly go to the chat
					noti.addEventListener('click', () => renderAndShowMainWindow(msg.roomId));
					noti.addEventListener('reply', ({response}) => sendMessage(response, msg.roomId));
				});
			}
		}
	});
}

function insertNewMessages(toInsert){
	messagesStorage.insert(toInsert);
}

function getMessagesForRoom(roomId){
	var reqBody = { "roomId" : roomId };
	var messagesRequest = getRequestObject('https://api.ciscospark.com/v1/messages', reqBody, 'GET');
	messagesRequest.done(function(messagesFromAPI, textStatus, xhr){
		console.log(messagesFromAPI);
		console.log(xhr.getResponseHeader('Link'));
		//Find all of the messages for this room, sorted by created date to match the API response
		messagesStorage.find({ roomId: roomId }).sort({created: -1}).exec(function (err, docs) {
			//Check if the count stored is equal to the API result, if not insert new ones
			if (docs.length < messagesFromAPI.items.length){
				//Splice the array starting at what we have saved - should be in the same order so only insert new messages
				var messageDifferenceCount = messagesFromAPI.items.length - docs.length;
				//Get as many messages as there are missing
				var unsavedMessages = messagesFromAPI.items.splice(0, messageDifferenceCount);
				//Insert all of the new messages
				messagesStorage.insert(unsavedMessages);
			}
		});
  })
  .fail(function(xhr){
    console.log(xhr);
  });
}

function getAllUsersInRoom(roomId, url){
	var reqBody = { "roomId" : roomId };
	//Use base URL if null, if it's provided it's likely paged
	if (url == null){
		url = "https://api.ciscospark.com/v1/memberships";
	} else {
		//If it isn't null, we don't need a body due to paging
		reqBody = null;
	}
	var usersRequest = getRequestObject(url, reqBody, 'GET');
	usersRequest.done(function(userFromAPI, textStatus, xhr){
		//Upsert the returned users
		var myId = sessionStorage.getItem("logged_in_user_id");
		userFromAPI.items.forEach(function(user){
			if (user.id != myId){
				getUserById(user.personId, roomId);
			}
			//usersStorage.update({personId: user.personId }, user, { upsert: true });
		});
	})
	.fail(function(xhr){
		console.log(xhr);
	})
}

function getRoomMemberships(roomId){
	var reqBody = { "roomId" : roomId };
	var usersRequest = getRequestObject('https://api.ciscospark.com/v1/memberships', reqBody, 'GET');
	usersRequest.done(function(userFromAPI, textStatus, xhr){
		console.log(userFromAPI);
		//If there are only two users, it's likely a direct room
		var myId = sessionStorage.getItem("logged_in_user_id");
		if (userFromAPI.items.length == 2){
			userFromAPI.items.forEach(function(m){
				var isMe = (myId == m.personId);
				//Render them to the top bar
				if (!isMe){
					$("#header-person-name").html(m.personDisplayName);
					//Get them from cached users for their icon and other details
					usersStorage.find({ id: m.personId }, function (err, docs) {
						$("#header-person-photo").html("<img class='user-avatar-image' src='"+docs[0].avatar+"'/>");
					});
				}
			});
		} else {
			//Just show the room name
			renderRoomNameAndDetails(roomId);
		}
	})
	.fail(function(xhr){
		console.log(xhr);
	})
}

function getUserById(userId, roomId){
	var reqBody = { "id" : userId };
	var usersRequest = getRequestObject('https://api.ciscospark.com/v1/people/' + userId, null, 'GET');
	usersRequest.done(function(userFromAPI, textStatus, xhr){
		usersStorage.update({id : userFromAPI.id}, userFromAPI, { upsert: true });
		//If they are in the side, update their active dot
		if (($("#" + roomId + "-active-dot").length != 0)){
			//Get the current class
			var currentClass = $("#" + roomId + "-active-dot").attr('class');
			var setNewClass = false;
			if (currentClass != null && currentClass != ''){
				//Check if the current status needs to be replaced
				if ((currentClass == "user-active-dot" && userFromAPI.status == "active") || (currentClass == "user-inactive-dot" && userFromAPI.status != "active")){
						$("#" + roomId + "-active-dot").removeClass();
						setNewClass = true;
				}
			} else {
				setNewClass = true;
			}
			if (setNewClass){
				if (userFromAPI.status == "active"){
					$("#" + roomId + "-active-dot").addClass("user-active-dot");
				} else {
					$("#" + roomId + "-active-dot").addClass("user-inactive-dot");
				}
			}
		}
	})
	.fail(function(xhr){
		console.log(xhr);
	})
}

roomsStorage.find({}).sort({created: -1}).exec(function (err, docs) {
	console.log(docs);
	//For every room, get the members names to be used in later chats
	renderRooms(docs);
});
