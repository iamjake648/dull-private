function getAuthRequestObject(data){
	return jqXhr = $.ajax({
      		url: "https://api.ciscospark.com/v1/access_token",
      		async : false,
      		contentType: 'application/x-www-form-urlencoded',
      		dataType: 'json',
      		type: 'POST',
      		data: data
    });
}

//Returns an ajax object to be used in getting data
function getRequestObject(url, data, method){
	if (data != null) {
    	return jqXhr = $.ajax({
      		url: url,
      		headers: { 'Authorization': 'Bearer '+sessionStorage.getItem("accessToken") },
      		contentType: 'application/json',
      		dataType: 'json',
      		type: method,
      		data: data
    	});
  	} else {
    	return jqXhr = $.ajax({
      		url: url,
      		headers: { 'Authorization': 'Bearer '+sessionStorage.getItem("accessToken") },
      		contentType: 'application/json',
      		dataType: 'json',
      		type: method
    	});
  	}
}
