//Used to summarize links
var extractor = require('unfluff');
var summaryTool = require('node-summary');
var request = require("request");

function getYouTubeEmbedCode(url) {
    var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
    var match = url.match(regExp);
    if (match && match[2].length == 11) {
        return '<iframe width="560" height="315" src="https://www.youtube.com/embed/' + match[2] + '" frameborder="0" allowfullscreen></iframe>';
    } else {
				//Couldn't find it, just return the regular link
        return url;
    }
}

function getVideoVimeoURL(url){
    var vimeoRegex = /(?:vimeo)\.com.*(?:videos|video|channels|)\/([\d]+)/i;
    var parsed = url.match(vimeoRegex);
    return "<iframe src='//player.vimeo.com/video/" + parsed[1] + "'allowfullscreen></iframe>";
}

function getLinkFromMessage(messageContent){
	var split = messageContent.split(' ');
	split.forEach(function(word){
		if (word.startsWith('https://')){
			return word;
		}
	})
}

function regularLinkTester(url,id) {
    //Wait five seconds
    var timeout = 5000;
    var timedOut = false, timer;
    var img = new Image();
    img.onerror = img.onabort = function() {
        if (!timedOut) {
            clearTimeout(timer);
            regularLinkParserCallback(url, "error",id);
        }
    };
    img.onload = function() {
        if (!timedOut) {
            clearTimeout(timer);
            regularLinkParserCallback(url, "success",id);
        }
    };
    img.src = url;
    timer = setTimeout(function() {
        timedOut = true;
        regularLinkParserCallback(url, "timeout",id);
    }, timeout);
}

function regularLinkParserCallback(link, imageStatus,id){
  if (imageStatus == "success"){
    $("#" + id).append(getSpecialContentDiv('image',id,getImageFromUrl(link)));
    $("#messages-pane").scrollTop($("#messages-pane")[0].scrollHeight);

  } else {
    //Try to append a link summary
    appendLinkSummary(id,link);
  }
}

function getImageFromUrl(url){
	return "<img class='chat-message-image' src='"+url+"'/>";
}

function appendLinkSummary(id,url){
  //Get content from the link
  request({
      uri: url,
    },
    function(error, response, body) {
       //Use unfluff to break up the parts we care about
  	   data = extractor(body);
       //Build a summary of the link
       summaryTool.summarize(data.title, data.text, function(err, summary) {
         if(err) console.log("Something went wrong man!");
         //Built footer
         var footer = data.softTitle;
         if (data.author != null && data.author != ''){
           footer += " Written by " + data.author + " At: " + data.date;
         }
         if (data.image != null || data.image != ''){
           $("#" + id).append(getSpecialContentDiv('link', id, getSummaryDiv(url, summary, footer, data.image, id)));
         } else {
           $("#" + id).append(getSpecialContentDiv('link', id, getSummaryDiv(url, summary, footer, null, id)));
         }
         $("#messages-pane").scrollTop($("#messages-pane")[0].scrollHeight);
  	});
  });
}
