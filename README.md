# dull-private
Private Develop Repo for Dull. Everything being worked on is in the Projects tab. 

# running and important files 
 - TO Run: `npm install` (once) and then `npm start` While you can view the stock html and css files without running through an electron app, it's highly recommended as a lot of plugins depend on electron. 
 - index.html - main chat app view
 - scripts/view.js - generates divs for various chat elements
 - editor.css - stylesheet for the app
 - scripts/api.js - wrapper for ajax requests
 - scripts/keybindings.js - keybindings for various functions
 - scripts/scripts.js - controls the flow of the application
