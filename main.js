const {app, BrowserWindow} = require('electron')
  const path = require('path')
  const {ipcMain} = require('electron')
  const url = require('url')
  var Datastore = require('nedb')
  , settings = new Datastore({ filename: 'dull-settings', autoload: true });


  // Keep a global reference of the window object, if you don't, the window will
  // be closed automatically when the JavaScript object is garbage collected.
  let win
  let login

  function createChatWindow (apiKey) {
    // Create the browser window. Web Security is off to allow CORS
    win = new BrowserWindow({width: 1300, height: 900, icon: path.join(__dirname, 'icon.ico'), webPreferences: { webSecurity : false}})
  	var loadSettings = { "apiKey" : apiKey };
    // and load the index.html of the app.
    win.loadURL(url.format({
      pathname: path.join(__dirname, 'index.html'),
      protocol: 'file:',
      slashes: true,
      hash: encodeURIComponent(JSON.stringify(loadSettings))
    }))

    //Listen for notifications
    ipcMain.on("notification-data", function(event, arg) {
      //Set app badge icon
      app.dock.bounce();
      app.setBadgeCount(arg.length);
      console.log(arg);
    });

    //Listen for requests to show the main window
    ipcMain.on("show-window", function(event, arg) {
      win.show();
    });

    // Open the DevTools.
    win.webContents.openDevTools()

    // Emitted when the window is closed.
    win.on('closed', () => {
      win = null
    })
    //Show native notifications
    win.on('blur', () => {
      win.webContents.executeJavaScript('window.clientOpen = false;');
    })
    win.on('hide', () => {
      win.webContents.executeJavaScript('window.clientOpen = false;');
    })
    //Don't show native notifications
    win.on('focus', () => {
      win.webContents.executeJavaScript('window.clientOpen = true;');
    })
    win.on('show', () => {
      win.webContents.executeJavaScript('window.clientOpen = true;');
    })
  }

  function getRandomString(length) {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < length; i++){
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
  }

  function createLoginWindow() {
  	login = new BrowserWindow({width: 700, height: 800, show: false, icon: __dirname + '/icon.ico',  webPreferences: {nodeIntegration:false, webSecurity : false}});
  	//Forward to Spark for OAuth
  	var clientId = 'C2847889489e42e88cc62902e727a72a0da862e08f400b4851af92efabe346c54';
    var stateCode = getRandomString(12);
  	//Authroize the dull client with full permissions to avoid the 'on behalf of' messaging in the client. State must match to ensure it wasn't tampered with
  	login.loadURL('https://api.ciscospark.com/v1/authorize?client_id='+clientId+'&response_type=code&redirect_uri=http%3A%2F%2Flocalhost%2Foauthsuccess&scope=spark%3Aall%20spark%3Akms&state=' + stateCode);
  	login.webContents.on('did-get-redirect-request', function (event, oldUrl, newUrl) {
  		//If it went to localhost, then we need to find the client id
  		if (newUrl.includes("http://localhost/oauthsuccess")){
        if (newUrl.includes(stateCode)){
          login.hide();
    			//Get the token
    			var code = newUrl.substring(newUrl.indexOf('=')+1, newUrl.indexOf('&'));
    			//Save the API Key
    			var setting = { "type" : "api_key", "value" : code};
    			settings.insert(setting);
    			createChatWindow(code);
        } else {
          console.log('Invalid State');
        }
  		}
	});
	login.show();
  }

  function init(){
    createLoginWindow();
  	//Check if we have an API Key
    // settings.find({ type: 'api_key' }).sort({ planet: 1 }).exec(function (err, docs) {
    //   if (docs.length == 1 && docs[0].value != null){
  	// 		createChatWindow(docs[0].value);
  	// 	} else {
  	// 		createLoginWindow();
  	// 	}
    // });
  }

  // This method will be called when Electron has finished
  // initialization and is ready to create browser windows.
  // Some APIs can only be used after this event occurs.
  app.on('ready', init)

  // Quit when all windows are closed.
  app.on('window-all-closed', () => {
    // On macOS it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
      //TODO: spawn invisible window that handles updating messages in the background
      //app.quit()
    }
  })

  app.on('activate', () => {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (win === null) {
      createChatWindow()
    }
    //Remove notifications
    app.setBadgeCount(0);
  })
