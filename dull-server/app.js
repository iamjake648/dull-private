// Load .env file into Environment Variables
require('dotenv').config();

var express = require('express');
var bodyParser = require('body-parser');
var cors = require('cors');
var compression = require('compression');
var helmet = require('helmet');
var jwt = require('express-jwt');
var jwttoken = require('jsonwebtoken');
var cors = require('cors');
var exec = require('child_process').exec;

//Check for a JWT Key, if not set to 'local' for tests
if (!process.env.JWT_KEY){
  process.env.JWT_KEY = 'local';
}
//Sets up jwt object to verify key in auth header
var jwtCheck = jwt({
  secret: process.env.JWT_KEY
});

var app = module.exports = express();
app.use('/emoticon-files/', express.static('emoticons'))

//Basic application hardening
app.use(helmet());
// parse application/json
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

//Allow cross origin requests
app.use(cors());
app.use(compression()); //Compress all routes

//Setup routes
app.use('/emoticons', require('./controllers/emoticons'));
app.use('/pgpkeys', require('./controllers/pgpkeys'));
app.use('/shortcuts', require('./controllers/textExpanders'));

app.get('/', function(req, res) {
  res.send({
    status: 'success'
  });
});

if (!module.parent) {
  var db = require('./common/db.js');
  db.connect(function(err) {
    if (err) {
      console.log('Unable to connect to database.');
      process.exit(1);
    } else {
      var port = process.env.PORT || 3000;
      app.listen(port, function() {
        console.log('Express started on port ' + port);
      });
    }
  });
}
