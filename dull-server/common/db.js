var mysql = require('mysql');
var state = {
  pool: null
}

exports.connect = function(callback) {
  state.pool = mysql.createPool({
    host     : process.env.MYSQL_HOST,
    user     : process.env.MYSQL_USER,
    password : process.env.MYSQL_PASS,
    database : process.env.MYSQL_DB
  });

  callback();
}

exports.get = function() {
  return state.pool;
}
